using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declencheur : MonoBehaviour
{
    int compteur;

    // Start is called before the first frame update
    void Start()
    {
        compteur = 0;
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnTriggerEnter(Collider other)
    {
        compteur++;
        Debug.Log("Bloc entrant! bloc(s) dans la zone de stockage: " + compteur);
    }

    private void OnTriggerExit(Collider other)
    {
        compteur--;
        Debug.Log("Bloc sortant! bloc(s) dans la zone de stockage: " + compteur);
    }
}